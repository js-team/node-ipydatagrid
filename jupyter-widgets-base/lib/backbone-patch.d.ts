import { ModelSetOptions } from 'backbone';
export declare function set(key: string | {}, val: any, options: ModelSetOptions & {
    unset?: boolean;
}): any;
//# sourceMappingURL=backbone-patch.d.ts.map