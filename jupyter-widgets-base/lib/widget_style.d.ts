/// <reference types="backbone" />
import { WidgetModel, WidgetView } from './widget';
/**
 * Three functions to deal with some CSS attributes
 * to make them easier to use.
 */
export declare class StyleModel extends WidgetModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        [s: string]: IStyleProperty;
    };
}
interface IStyleProperty {
    attribute: string;
    selector: string;
    default: string;
}
export declare class StyleView extends WidgetView {
    /**
     * Public constructor
     */
    initialize(parameters: WidgetView.IInitializeParameters): void;
    /**
     * Register a CSS trait that is known by the model
     * @param trait
     */
    registerTrait(trait: string): void;
    /**
     * Handles when a trait value changes
     */
    handleChange(trait: string, value: any): void;
    /**
     * Apply styles for all registered traits
     */
    style(): void;
    /**
     * Remove the styling from the parent view.
     */
    unstyle(): void;
    private _traitNames;
}
export {};
//# sourceMappingURL=widget_style.d.ts.map