// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.
import { Token } from '@lumino/coreutils';
/**
 * A runtime interface token for a widget registry.
 */
export const IJupyterWidgetRegistry = new Token('jupyter.extensions.jupyterWidgetRegistry');
//# sourceMappingURL=registry.js.map