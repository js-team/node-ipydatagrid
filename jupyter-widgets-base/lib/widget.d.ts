import * as utils from './utils';
import * as Backbone from 'backbone';
import { NativeView } from './nativeview';
import { JSONObject, JSONValue } from '@lumino/coreutils';
import { Message } from '@lumino/messaging';
import { Widget, Panel } from '@lumino/widgets';
import { LayoutModel } from './widget_layout';
import { StyleModel } from './widget_style';
import { IWidgetManager } from './manager';
import { IClassicComm, ICallbacks } from './services-shim';
import { BufferJSON, Dict } from './utils';
import { KernelMessage } from '@jupyterlab/services';
/**
 * Replace model ids with models recursively.
 */
export declare function unpack_models(value: any | Dict<unknown> | string | (Dict<unknown> | string)[], manager?: IWidgetManager): Promise<WidgetModel | Dict<WidgetModel> | WidgetModel[] | any>;
/** Replace models with ids recursively.
 *
 * If the commonly-used `unpack_models` is given as the `deseralize` method,
 * pack_models would be the appropriate `serialize`.
 * However, the default serialize method will have the same effect, when
 * `unpack_models` is used as the deserialize method.
 * This is to ensure backwards compatibility, see:
 *   https://github.com/jupyter-widgets/ipywidgets/pull/3738/commits/f9e27328bb631eb5247a7a6563595d3e655492c7#diff-efb19099381ae8911dd7f69b015a0138d08da7164512c1ee112aa75100bc9be2
 */
export declare function pack_models(value: WidgetModel | Dict<WidgetModel> | WidgetModel[] | any, widget?: WidgetModel): any | Dict<unknown> | string | (Dict<unknown> | string)[];
/**
 * Type declaration for general widget serializers.
 */
export interface ISerializers {
    [key: string]: {
        deserialize?: (value?: any, manager?: IWidgetManager) => any;
        serialize?: (value?: any, widget?: WidgetModel) => any;
    };
}
export interface IBackboneModelOptions extends Backbone.ModelSetOptions {
    model_id: string;
    comm?: any;
    widget_manager: any;
}
export declare class WidgetModel extends Backbone.Model {
    /**
     * The default attributes.
     */
    defaults(): Backbone.ObjectHash;
    /**
     * Test to see if the model has been synced with the server.
     *
     * #### Notes
     * As of backbone 1.1, backbone ignores `patch` if it thinks the
     * model has never been pushed.
     */
    isNew(): boolean;
    /**
     * Constructor
     *
     * Initializes a WidgetModel instance. Called by the Backbone constructor.
     *
     * Parameters
     * ----------
     * widget_manager : WidgetManager instance
     * model_id : string
     *      An ID unique to this model.
     * comm : Comm instance (optional)
     */
    initialize(attributes: Backbone.ObjectHash, options: IBackboneModelOptions): void;
    get comm_live(): boolean;
    set comm_live(x: boolean);
    /**
     * Send a custom msg over the comm.
     */
    send(content: JSONValue, callbacks?: ICallbacks, buffers?: ArrayBuffer[] | ArrayBufferView[]): void;
    /**
     * Close model
     *
     * @param comm_closed - true if the comm is already being closed. If false, the comm will be closed.
     *
     * @returns - a promise that is fulfilled when all the associated views have been removed.
     */
    close(comm_closed?: boolean): Promise<void>;
    /**
     * Handle when a widget comm is closed.
     */
    _handle_comm_closed(msg: KernelMessage.ICommCloseMsg): void;
    /**
     * Handle incoming comm msg.
     */
    _handle_comm_msg(msg: KernelMessage.ICommMsgMsg): Promise<void>;
    /**
     * Handle when a widget is updated from the backend.
     *
     * This function is meant for internal use only. Values set here will not be propagated on a sync.
     */
    set_state(state: Dict<unknown>): void;
    /**
     * Get the serializable state of the model.
     *
     * If drop_default is truthy, attributes that are equal to their default
     * values are dropped.
     */
    get_state(drop_defaults?: boolean): JSONObject;
    /**
     * Handle status msgs.
     *
     * execution_state : ('busy', 'idle', 'starting')
     */
    _handle_status(msg: KernelMessage.IStatusMsg): void;
    /**
     * Create msg callbacks for a comm msg.
     */
    callbacks(view?: WidgetView): ICallbacks;
    /**
     * Set one or more values.
     *
     * We just call the super method, in which val and options are optional.
     * Handles both "key", value and {key: value} -style arguments.
     */
    set(key: any, val?: any, options?: any): any;
    /**
     * Handle sync to the back-end.  Called when a model.save() is called.
     *
     * Make sure a comm exists.
     *
     * Parameters
     * ----------
     * method : create, update, patch, delete, read
     *   create/update always send the full attribute set
     *   patch - only send attributes listed in options.attrs, and if we
     *   are queuing up messages, combine with previous messages that have
     *   not been sent yet
     * model : the model we are syncing
     *   will normally be the same as `this`
     * options : dict
     *   the `attrs` key, if it exists, gives an {attr: value} dict that
     *   should be synced, otherwise, sync all attributes.
     *
     */
    sync(method: string, model: WidgetModel, options?: any): any;
    rememberLastUpdateFor(msgId: string): void;
    /**
     * Serialize widget state.
     *
     * A serializer is a function which takes in a state attribute and a widget,
     * and synchronously returns a JSONable object. The returned object will
     * have toJSON called if possible, and the final result should be a
     * primitive object that is a snapshot of the widget state that may have
     * binary array buffers.
     */
    serialize(state: Dict<any>): JSONObject;
    /**
     * Send a sync message to the kernel.
     *
     * If a message is sent successfully, this returns the message ID of that
     * message. Otherwise it returns an empty string
     */
    send_sync_message(state: JSONObject, callbacks?: any): string;
    /**
     * Push this model's state to the back-end
     *
     * This invokes a Backbone.Sync.
     */
    save_changes(callbacks?: {}): void;
    /**
     * on_some_change(['key1', 'key2'], foo, context) differs from
     * on('change:key1 change:key2', foo, context).
     * If the widget attributes key1 and key2 are both modified,
     * the second form will result in foo being called twice
     * while the first will call foo only once.
     */
    on_some_change(keys: string[], callback: (...args: any[]) => void, context: any): void;
    /**
     * Serialize the model.  See the deserialization function at the top of this file
     * and the kernel-side serializer/deserializer.
     */
    toJSON(options?: {}): string;
    /**
     * Returns a promise for the deserialized state. The second argument
     * is an instance of widget manager, which is required for the
     * deserialization of widget models.
     */
    static _deserialize_state(state: Dict<BufferJSON>, manager: IWidgetManager): Promise<utils.Dict<unknown>>;
    static serializers: ISerializers;
    widget_manager: IWidgetManager;
    model_id: string;
    views?: {
        [key: string]: Promise<WidgetView>;
    };
    state_change: Promise<any>;
    comm?: IClassicComm;
    name: string;
    module: string;
    private _comm_live;
    private _closed;
    private _state_lock;
    private _buffered_state_diff;
    private _buffered_state_diff_synced;
    private _msg_buffer;
    private _msg_buffer_callbacks;
    private _pending_msgs;
    private _expectedEchoMsgIds;
    private _attrsToUpdate;
}
export declare class DOMWidgetModel extends WidgetModel {
    static serializers: ISerializers;
    defaults(): Backbone.ObjectHash;
}
export declare class WidgetView extends NativeView<WidgetModel> {
    /**
     * Public constructor.
     */
    constructor(options?: Backbone.ViewOptions<WidgetModel> & {
        options?: any;
    });
    /**
     * Initializer, called at the end of the constructor.
     */
    initialize(parameters: WidgetView.IInitializeParameters): void;
    /**
     * Handle message sent to the front end.
     *
     * Used to focus or blur the widget.
     */
    handle_message(content: any): void;
    /**
     * Triggered on model change.
     *
     * Update view to be consistent with this.model
     */
    update(options?: any): void;
    /**
     * Render a view
     *
     * @returns the view or a promise to the view.
     */
    render(): any;
    /**
     * Create and promise that resolves to a child view of a given model
     */
    create_child_view<VT extends DOMWidgetView = DOMWidgetView>(child_model: DOMWidgetModel, options?: any): Promise<VT>;
    create_child_view<VT extends WidgetView = WidgetView>(child_model: WidgetModel, options?: any): Promise<VT>;
    /**
     * Create msg callbacks for a comm msg.
     */
    callbacks(): ICallbacks;
    /**
     * Send a custom msg associated with this view.
     */
    send(content: {}, buffers?: ArrayBuffer[] | ArrayBufferView[]): void;
    touch(): void;
    remove(): any;
    options: any;
    /**
     * A promise that resolves to the parent view when a child view is displayed.
     */
    displayed: Promise<WidgetView>;
}
export declare namespace WidgetView {
    interface IInitializeParameters<T extends WidgetModel = WidgetModel> extends Backbone.ViewOptions<T> {
        options: any;
    }
}
export declare namespace JupyterLuminoWidget {
    interface IOptions {
        view: DOMWidgetView;
    }
}
export declare class JupyterLuminoWidget extends Widget {
    constructor(options: Widget.IOptions & JupyterLuminoWidget.IOptions);
    /**
     * Dispose the widget.
     *
     * This causes the view to be destroyed as well with 'remove'
     */
    dispose(): void;
    /**
     * Process the Lumino message.
     *
     * Any custom Lumino widget used inside a Jupyter widget should override
     * the processMessage function like this.
     */
    processMessage(msg: Message): void;
    private _view;
}
/**
 * @deprecated Use {@link JupyterLuminoWidget} instead (Since 8.0).
 */
export declare const JupyterPhosphorWidget: typeof JupyterLuminoWidget;
export declare class JupyterLuminoPanelWidget extends Panel {
    constructor(options: JupyterLuminoWidget.IOptions & Panel.IOptions);
    /**
     * Process the Lumino message.
     *
     * Any custom Lumino widget used inside a Jupyter widget should override
     * the processMessage function like this.
     */
    processMessage(msg: Message): void;
    /**
     * Dispose the widget.
     *
     * This causes the view to be destroyed as well with 'remove'
     */
    dispose(): void;
    private _view;
}
/**
 * @deprecated Use {@link JupyterLuminoPanelWidget} instead (Since 8.0).
 */
export declare const JupyterPhosphorPanelWidget: typeof JupyterLuminoPanelWidget;
export declare class DOMWidgetView extends WidgetView {
    /**
     * Public constructor
     */
    initialize(parameters: WidgetView.IInitializeParameters): void;
    setLayout(layout: LayoutModel, oldLayout?: LayoutModel): void;
    setStyle(style: StyleModel, oldStyle?: StyleModel): void;
    updateTooltip(): void;
    /**
     * Update the DOM classes applied to an element, default to this.el.
     */
    update_classes(old_classes: string[], new_classes: string[], el?: HTMLElement): void;
    /**
     * Update the DOM classes applied to the widget based on a single
     * trait's value.
     *
     * Given a trait value classes map, this function automatically
     * handles applying the appropriate classes to the widget element
     * and removing classes that are no longer valid.
     *
     * Parameters
     * ----------
     * class_map: dictionary
     *  Dictionary of trait values to class lists.
     *  Example:
     *      {
     *          success: ['alert', 'alert-success'],
     *          info: ['alert', 'alert-info'],
     *          warning: ['alert', 'alert-warning'],
     *          danger: ['alert', 'alert-danger']
     *      };
     * trait_name: string
     *  Name of the trait to check the value of.
     * el: optional DOM element handle, defaults to this.el
     *  Element that the classes are applied to.
     */
    update_mapped_classes(class_map: Dict<string[]>, trait_name: string, el?: HTMLElement): void;
    set_mapped_classes(class_map: Dict<string[]>, trait_name: string, el?: HTMLElement): void;
    _setElement(el: HTMLElement): void;
    remove(): any;
    processLuminoMessage(msg: Message): void;
    private _comm_live_update;
    updateTabindex(): void;
    /**
     * @deprecated Use {@link luminoWidget} instead (Since 8.0).
     */
    get pWidget(): Widget;
    /**
     * @deprecated Use {@link luminoWidget} instead (Since 8.0).
     */
    set pWidget(value: Widget);
    el: HTMLElement;
    '$el': any;
    luminoWidget: Widget;
    layoutPromise: Promise<any>;
    stylePromise: Promise<any>;
}
//# sourceMappingURL=widget.d.ts.map