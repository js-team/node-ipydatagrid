// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.
import { DOMWidgetModel, DOMWidgetView, StyleModel, } from '@jupyter-widgets/base';
import { typeset } from './utils';
import { JUPYTER_CONTROLS_VERSION } from './version';
export class DescriptionStyleModel extends StyleModel {
    defaults() {
        return Object.assign(Object.assign({}, super.defaults()), { _model_name: 'DescriptionStyleModel', _model_module: '@jupyter-widgets/controls', _model_module_version: JUPYTER_CONTROLS_VERSION });
    }
}
DescriptionStyleModel.styleProperties = {
    description_width: {
        selector: '.widget-label',
        attribute: 'width',
        default: null,
    },
};
export class DescriptionModel extends DOMWidgetModel {
    defaults() {
        return Object.assign(Object.assign({}, super.defaults()), { _model_name: 'DescriptionModel', _view_name: 'DescriptionView', _view_module: '@jupyter-widgets/controls', _model_module: '@jupyter-widgets/controls', _view_module_version: JUPYTER_CONTROLS_VERSION, _model_module_version: JUPYTER_CONTROLS_VERSION, description: '', description_allow_html: false });
    }
}
export class DescriptionView extends DOMWidgetView {
    render() {
        this.label = document.createElement('label');
        this.el.appendChild(this.label);
        this.label.className = 'widget-label';
        this.label.style.display = 'none';
        this.listenTo(this.model, 'change:description', this.updateDescription);
        this.listenTo(this.model, 'change:description_allow_html', this.updateDescription);
        this.listenTo(this.model, 'change:tabbable', this.updateTabindex);
        this.updateDescription();
        this.updateTabindex();
        this.updateTooltip();
    }
    typeset(element, text) {
        this.displayed.then(() => {
            var _a, _b, _c;
            if ((_b = (_a = window.MathJax) === null || _a === void 0 ? void 0 : _a.Hub) === null || _b === void 0 ? void 0 : _b.Queue) {
                return typeset(element, text);
            }
            const widget_manager = this.model.widget_manager;
            const latexTypesetter = (_c = widget_manager._rendermime) === null || _c === void 0 ? void 0 : _c.latexTypesetter;
            if (latexTypesetter) {
                if (text !== void 0) {
                    element.textContent = text;
                }
                latexTypesetter.typeset(element);
            }
        });
    }
    updateDescription() {
        const description = this.model.get('description');
        if (description.length === 0) {
            this.label.style.display = 'none';
        }
        else {
            if (this.model.get('description_allow_html')) {
                this.label.innerHTML =
                    this.model.widget_manager.inline_sanitize(description);
            }
            else {
                this.label.textContent = description;
            }
            this.typeset(this.label);
            this.label.style.display = '';
        }
    }
    updateTooltip() {
        if (!this.label)
            return;
        this.label.title = this.model.get('tooltip');
    }
}
/**
 * For backwards compatibility with jupyter-js-widgets 2.x.
 *
 * Use DescriptionModel instead.
 */
export class LabeledDOMWidgetModel extends DescriptionModel {
}
/**
 * For backwards compatibility with jupyter-js-widgets 2.x.
 *
 * Use DescriptionView instead.
 */
export class LabeledDOMWidgetView extends DescriptionView {
}
//# sourceMappingURL=widget_description.js.map