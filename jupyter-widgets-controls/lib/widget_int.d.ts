/// <reference types="backbone" />
import { CoreDescriptionModel } from './widget_core';
import { DescriptionStyleModel, DescriptionView } from './widget_description';
import { DOMWidgetView, WidgetView } from '@jupyter-widgets/base';
export declare class IntModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class BoundedIntModel extends IntModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SliderStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        handle_color: {
            selector: string;
            attribute: string;
            default: any;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class IntSliderModel extends BoundedIntModel {
    defaults(): Backbone.ObjectHash;
    initialize(attributes: Backbone.ObjectHash, options: {
        model_id: string;
        comm: any;
        widget_manager: any;
    }): void;
    update_readout_format(): void;
    readout_formatter: any;
}
export declare class IntRangeSliderModel extends IntSliderModel {
}
export declare abstract class BaseIntSliderView extends DescriptionView {
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    /**
     * Returns true if the readout box content overflows.
     */
    readout_overflow(): boolean;
    /**
     * Write value to a string
     */
    abstract valueToString(value: number | number[]): string;
    /**
     * Parse value from a string
     */
    abstract stringToValue(text: string): number | number[] | null;
    events(): {
        [e: string]: string;
    };
    handleKeyDown(e: KeyboardEvent): void;
    /**
     * Create a new noUiSlider object
     */
    createSlider(): void;
    /**
     * Recreate/Regenerate a slider object
     * noUiSlider does not support in-place mutation of the orientation
     * state. We therefore need to destroy the current instance
     * and create a new one with the new properties. This is
     * handled in a separate function and has a dedicated event
     * handler.
     */
    regenSlider(e: any): void;
    /**
     * Update noUiSlider object in-place with new options
     */
    abstract updateSliderOptions(e: any): void;
    /**
     * Update noUiSlider's state so that it is
     * synced with the Backbone.jas model
     */
    abstract updateSliderValue(model: any, value: any, options: any): void;
    /**
     * this handles the entry of text into the contentEditable label first, the
     * value is checked if it contains a parseable value then it is clamped
     * within the min-max range of the slider finally, the model is updated if
     * the value is to be changed
     *
     * if any of these conditions are not met, the text is reset
     */
    abstract handleTextChange(): void;
    /**
     * Called when the slider handle is released after dragging,
     * or by tapping or moving by the arrow keys.
     */
    abstract handleSliderChangeEvent(value: any, handle: any): void;
    /**
     * Called whilst the slider is dragged, tapped or moved by the arrow keys.
     */
    abstract handleSliderUpdateEvent(value: any, handle: any): void;
    /**
     * Called when the slider value has changed.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    abstract handleSliderChanged(values: number[], handle: number): void;
    /**
     * Validate the value of the slider before sending it to the back-end
     * and applying it to the other views on the page.
     */
    _validate_slide_value(x: number): number;
    $slider: any;
    slider_container: HTMLElement;
    readout: HTMLDivElement;
    model: IntSliderModel;
    _parse_value: typeof parseInt;
}
export declare class IntRangeSliderView extends BaseIntSliderView {
    update(options?: any): void;
    /**
     * Write value to a string
     */
    valueToString(value: number[]): string;
    /**
     * Parse value from a string
     */
    stringToValue(text: string | null): number[] | null;
    handleTextChange(): void;
    /**
     * Called when the slider handle is released after dragging,
     * or by tapping or moving by the arrow keys.
     */
    handleSliderChangeEvent(values: any, handle: any): void;
    /**
     * Called whilst the slider is dragged, tapped or moved by the arrow keys.
     */
    handleSliderUpdateEvent(values: any, handle: any): void;
    handleSliderChanged(values: number[], handle: number): void;
    updateSliderOptions(e: any): void;
    updateSliderValue(model: any, _: any, options: any): void;
    _range_regex: RegExp;
}
export declare class IntSliderView extends BaseIntSliderView {
    update(options?: any): void;
    valueToString(value: number | number[]): string;
    stringToValue(text: string): number | number[];
    handleTextChange(): void;
    handleSliderChangeEvent(values: any, handle: any): void;
    handleSliderUpdateEvent(values: any, handle: any): void;
    handleSliderChanged(values: any, handle: any): void;
    updateSliderOptions(e: any): void;
    updateSliderValue(model: any, _: any, options: any): void;
}
export declare class IntTextModel extends IntModel {
    defaults(): Backbone.ObjectHash;
}
export declare class BoundedIntTextModel extends BoundedIntModel {
    defaults(): Backbone.ObjectHash;
}
export declare class IntTextView extends DescriptionView {
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle key down
     *
     * Stop propagation so the event isn't sent to the application.
     */
    handleKeyDown(e: KeyboardEvent): void;
    /**
     * Handles key press
     */
    handleKeypress(e: KeyboardEvent): void;
    /**
     * Handle key up
     */
    handleKeyUp(e: KeyboardEvent): void;
    /**
     * Call the submit handler if continuous update is true and we are not
     * obviously incomplete.
     */
    handleChanging(e: Event): void;
    /**
     * Applies validated input.
     */
    handleChanged(e: Event): void;
    _parse_value: typeof parseInt;
    _default_step: string;
    textbox: HTMLInputElement;
}
export declare class ProgressStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        bar_color: {
            selector: string;
            attribute: string;
            default: any;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class IntProgressModel extends BoundedIntModel {
    defaults(): Backbone.ObjectHash;
}
export declare class ProgressView extends DescriptionView {
    initialize(parameters: WidgetView.IInitializeParameters): void;
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
    update_bar_style(): void;
    set_bar_style(): void;
    progress: HTMLDivElement;
    bar: HTMLDivElement;
    static class_map: {
        success: string[];
        info: string[];
        warning: string[];
        danger: string[];
    };
}
export declare class PlayModel extends BoundedIntModel {
    defaults(): Backbone.ObjectHash;
    initialize(attributes: any, options: {
        model_id: string;
        comm: any;
        widget_manager: any;
    }): void;
    loop(): void;
    schedule_next(): void;
    stop(): void;
    pause(): void;
    animate(): void;
    play(): void;
    repeat(): void;
    private _timerId;
}
export declare class PlayView extends DOMWidgetView {
    render(): void;
    update(): void;
    onPlayingChanged(): void;
    updatePlaying(): void;
    updateRepeat(): void;
    playPauseButton: HTMLButtonElement;
    stopButton: HTMLButtonElement;
    repeatButton: HTMLButtonElement;
    model: PlayModel;
}
//# sourceMappingURL=widget_int.d.ts.map