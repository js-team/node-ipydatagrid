export { uuid, resolvePromisesDict } from '@jupyter-widgets/base';
/**
 * Apply MathJax rendering to an element, and optionally set its text.
 *
 * If MathJax is not available, make no changes.
 *
 * Parameters
 * ----------
 * element: Node
 * text: optional string
 */
export declare function typeset(element: HTMLElement, text?: string): void;
/**
 * escape text to HTML
 */
export declare function escape_html(text: string): string;
/**
 * Creates a wrappable Promise rejection function.
 */
export declare function reject(message: string, log: boolean): (error: Error) => never;
//# sourceMappingURL=utils.d.ts.map