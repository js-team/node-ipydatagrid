/// <reference types="backbone" />
import { CoreDOMWidgetModel } from './widget_core';
import { DOMWidgetView } from '@jupyter-widgets/base';
export declare class FileUploadModel extends CoreDOMWidgetModel {
    defaults(): Backbone.ObjectHash;
    static serializers: {
        value: {
            serialize: <T>(x: T) => T;
        };
    };
}
export declare class FileUploadView extends DOMWidgetView {
    el: HTMLButtonElement;
    fileInput: HTMLInputElement;
    preinitialize(): void;
    render(): void;
    update(): void;
    update_button_style(): void;
    set_button_style(): void;
    static class_map: {
        primary: string[];
        success: string[];
        info: string[];
        warning: string[];
        danger: string[];
    };
}
//# sourceMappingURL=widget_upload.d.ts.map