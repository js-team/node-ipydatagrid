/* This file has code derived from Lumino. The license for this Lumino code is:

Copyright (c) 2019 Project Jupyter Contributors
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Copyright (c) 2014-2017, PhosphorJS Contributors
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
import { MessageLoop } from '@lumino/messaging';
import { Signal } from '@lumino/signaling';
import { Platform } from '@lumino/domutils';
import { Panel, PanelLayout, TabBar, Widget } from '@lumino/widgets';
/**
 * A panel where visible widgets are stacked atop one another.
 *
 * #### Notes
 * This class provides a convenience wrapper around a [[PanelLayout]].
 */
export class EventedPanel extends Panel {
    constructor() {
        super(...arguments);
        this._widgetRemoved = new Signal(this);
    }
    /**
     * A signal emitted when a widget is removed from the panel.
     */
    get widgetRemoved() {
        return this._widgetRemoved;
    }
    /**
     * A message handler invoked on a `'child-removed'` message.
     */
    onChildRemoved(msg) {
        this._widgetRemoved.emit(msg.child);
    }
}
/**
 * A widget which combines a `TabBar` and a `EventedPanel`.
 *
 * #### Notes
 * This is a simple panel which handles the common case of a tab bar
 * placed next to a content area. The selected tab controls the widget
 * which is shown in the content area.
 *
 * For use cases which require more control than is provided by this
 * panel, the `TabBar` widget may be used independently.
 *
 * TODO: Support setting the direction??
 */
export class TabPanel extends Widget {
    /**
     * Construct a new tab panel.
     *
     * @param options - The options for initializing the tab panel.
     */
    constructor(options = {}) {
        super();
        this._currentChanged = new Signal(this);
        this.addClass('jupyter-widget-TabPanel');
        // Create the tab bar and contents panel.
        this.tabBar = new TabBar(options);
        this.tabBar.addClass('jupyter-widget-TabPanel-tabBar');
        this.tabContents = new EventedPanel();
        this.tabContents.addClass('jupyter-widget-TabPanel-tabContents');
        // Connect the tab bar signal handlers.
        this.tabBar.tabMoved.connect(this._onTabMoved, this);
        this.tabBar.currentChanged.connect(this._onCurrentChanged, this);
        this.tabBar.tabCloseRequested.connect(this._onTabCloseRequested, this);
        this.tabBar.tabActivateRequested.connect(this._onTabActivateRequested, this);
        // Connect the evented panel signal handlers.
        this.tabContents.widgetRemoved.connect(this._onWidgetRemoved, this);
        // Create the layout.
        const layout = new PanelLayout();
        // Add the child widgets to the layout.
        layout.addWidget(this.tabBar);
        layout.addWidget(this.tabContents);
        // Install the layout on the tab panel.
        this.layout = layout;
    }
    /**
     * A signal emitted when the current tab is changed.
     *
     * #### Notes
     * This signal is emitted when the currently selected tab is changed
     * either through user or programmatic interaction.
     *
     * Notably, this signal is not emitted when the index of the current
     * tab changes due to tabs being inserted, removed, or moved. It is
     * only emitted when the actual current tab node is changed.
     */
    get currentChanged() {
        return this._currentChanged;
    }
    /**
     * Get the index of the currently selected tab.
     *
     * #### Notes
     * This will be `null` if no tab is selected.
     */
    get currentIndex() {
        const currentIndex = this.tabBar.currentIndex;
        // Lumino tab bars have an index of -1 if no tab is selected
        return currentIndex === -1 ? null : currentIndex;
    }
    /**
     * Set the index of the currently selected tab.
     *
     * #### Notes
     * If the index is out of range, it will be set to `null`.
     */
    set currentIndex(value) {
        this.tabBar.currentIndex = value === null ? -1 : value;
    }
    /**
     * Get the currently selected widget.
     *
     * #### Notes
     * This will be `null` if there is no selected tab.
     */
    get currentWidget() {
        const title = this.tabBar.currentTitle;
        return title ? title.owner : null;
    }
    /**
     * Set the currently selected widget.
     *
     * #### Notes
     * If the widget is not in the panel, it will be set to `null`.
     */
    set currentWidget(value) {
        this.tabBar.currentTitle = value ? value.title : null;
    }
    /**
     * Get the whether the tabs are movable by the user.
     *
     * #### Notes
     * Tabs can always be moved programmatically.
     */
    get tabsMovable() {
        return this.tabBar.tabsMovable;
    }
    /**
     * Set the whether the tabs are movable by the user.
     *
     * #### Notes
     * Tabs can always be moved programmatically.
     */
    set tabsMovable(value) {
        this.tabBar.tabsMovable = value;
    }
    /**
     * A read-only array of the widgets in the panel.
     */
    get widgets() {
        return this.tabContents.widgets;
    }
    /**
     * Add a widget to the end of the tab panel.
     *
     * @param widget - The widget to add to the tab panel.
     *
     * #### Notes
     * If the widget is already contained in the panel, it will be moved.
     *
     * The widget's `title` is used to populate the tab.
     */
    addWidget(widget) {
        this.insertWidget(this.widgets.length, widget);
    }
    /**
     * Insert a widget into the tab panel at a specified index.
     *
     * @param index - The index at which to insert the widget.
     *
     * @param widget - The widget to insert into to the tab panel.
     *
     * #### Notes
     * If the widget is already contained in the panel, it will be moved.
     *
     * The widget's `title` is used to populate the tab.
     */
    insertWidget(index, widget) {
        if (widget !== this.currentWidget) {
            widget.hide();
        }
        this.tabContents.insertWidget(index, widget);
        this.tabBar.insertTab(index, widget.title);
    }
    /**
     * Handle the `currentChanged` signal from the tab bar.
     */
    _onCurrentChanged(sender, args) {
        // Extract the previous and current title from the args.
        const { previousIndex, previousTitle, currentIndex, currentTitle } = args;
        // Extract the widgets from the titles.
        const previousWidget = previousTitle ? previousTitle.owner : null;
        const currentWidget = currentTitle ? currentTitle.owner : null;
        // Hide the previous widget.
        if (previousWidget) {
            previousWidget.hide();
        }
        // Show the current widget.
        if (currentWidget) {
            currentWidget.show();
        }
        // Emit the `currentChanged` signal for the tab panel.
        this._currentChanged.emit({
            previousIndex,
            previousWidget,
            currentIndex,
            currentWidget,
        });
        // Flush the message loop on IE and Edge to prevent flicker.
        if (Platform.IS_EDGE || Platform.IS_IE) {
            MessageLoop.flush();
        }
    }
    /**
     * Handle the `tabActivateRequested` signal from the tab bar.
     */
    _onTabActivateRequested(sender, args) {
        args.title.owner.activate();
    }
    /**
     * Handle the `tabCloseRequested` signal from the tab bar.
     */
    _onTabCloseRequested(sender, args) {
        args.title.owner.close();
    }
    /**
     * Handle the `tabMoved` signal from the tab bar.
     */
    _onTabMoved(sender, args) {
        this.tabContents.insertWidget(args.toIndex, args.title.owner);
    }
    /**
     * Handle the `widgetRemoved` signal from the stacked panel.
     */
    _onWidgetRemoved(sender, widget) {
        this.tabBar.removeTab(widget.title);
    }
}
//# sourceMappingURL=tabpanel.js.map