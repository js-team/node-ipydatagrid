/// <reference types="backbone" />
import { ISerializers } from '@jupyter-widgets/base';
import { DescriptionView } from './widget_description';
import { CoreDescriptionModel } from './widget_core';
export interface ISerializedTime {
    /**
     * Integer hour (24H format)
     */
    hours: number;
    /**
     * Integer minutes
     */
    minutes: number;
    /**
     * Integer seconds
     */
    seconds: number;
    /**
     * Millisconds
     */
    milliseconds: number;
}
export declare function serialize_time(value: string): ISerializedTime | null;
export declare function deserialize_time(value: ISerializedTime): string | null;
export declare const time_serializers: {
    serialize: typeof serialize_time;
    deserialize: typeof deserialize_time;
};
export declare class TimeModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
    static serializers: ISerializers;
    static model_name: string;
    static view_name: string;
}
export declare class TimeView extends DescriptionView {
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed. The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update2(model?: Backbone.Model, options?: any): void;
    events(): {
        [e: string]: string;
    };
    private _update_value;
    private _picker_change;
    private _picker_focusout;
    private _timepicker;
}
//# sourceMappingURL=widget_time.d.ts.map