/// <reference types="backbone" />
import { CoreDOMWidgetModel } from './widget_core';
import { DOMWidgetView, Dict } from '@jupyter-widgets/base';
/**
 * Selection class which keeps track on selected indices.
 */
declare class Selection {
    constructor(start: number, dx: number, max: number);
    /**
     * Check if a given index is currently selected.
     */
    isSelected(index: number): boolean;
    /**
     * Update selection
     */
    updateSelection(dx: number): void;
    private start;
    private dx;
    private max;
}
declare class TagsInputBaseModel extends CoreDOMWidgetModel {
    defaults(): Backbone.ObjectHash;
}
declare abstract class TagsInputBaseView extends DOMWidgetView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed. The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
    /**
     * Update the auto-completion list
     */
    updateAutocomplete(): void;
    /**
     * Update the auto-completion list
     */
    updatePlaceholder(): void;
    /**
     * Update the tags, called when the selection has changed and we need to update the tags CSS
     */
    updateTags(): void;
    /**
     * Handle a new value is added from the input element
     */
    handleValueAdded(event: Event): void;
    /**
     * Add a new tag with a value of `tagValue` at the `index` position
     * Return true if the tag was correctly added, false otherwise
     */
    addTag(index: number, tagValue: string): boolean;
    /**
     * Resize the input element
     */
    resizeInput(): void;
    /**
     * Handle key events on the input element
     */
    handleKeyEvent(event: KeyboardEvent): void;
    /**
     * Function that gets called when a tag with a given `value` is being dragged.
     */
    ondragstart(event: DragEvent, index: number, tagValue: any, origin: string): void;
    /**
     * Function that gets called when a tag has been dragged on the tag at the `index` position.
     */
    ondrop(event: DragEvent, index: number): void;
    ondragover(event: DragEvent): void;
    ondragenter(event: DragEvent, index: number): void;
    ondragend(): void;
    /**
     * Select tags from `start` to `start + dx` not included.
     */
    select(start: number, dx: number): void;
    /**
     * Remove all the selected tags.
     */
    removeSelectedTags(): void;
    /**
     * Remove a tag given its index in the list
     */
    removeTag(tagIndex: number): void;
    /**
     * Focus on the input element
     */
    focus(): void;
    /**
     * Lose focus on the input element
     */
    loseFocus(): void;
    preinitialize(): void;
    /**
     * Validate an input tag typed by the user, returning the correct tag type. This should be overridden in subclasses.
     */
    validateValue(value: string): any;
    abstract createTag(value: any, index: number, selected: boolean): HTMLElement;
    abstract updateTag(tag: HTMLElement, value: any, index: number, selected: boolean): void;
    el: HTMLDivElement;
    taginputWrapper: HTMLDivElement;
    taginput: HTMLInputElement;
    autocompleteList: HTMLDataListElement;
    tags: HTMLElement[];
    hoveredTag: HTMLElement | null;
    hoveredTagIndex: number | null;
    datalistID: string;
    inputIndex: number;
    selection: null | Selection;
    preventLoosingFocus: boolean;
    model: TagsInputBaseModel;
}
export declare class TagsInputModel extends TagsInputBaseModel {
    defaults(): Backbone.ObjectHash;
}
export declare class TagsInputView extends TagsInputBaseView {
    /**
     * Create the string tag
     */
    createTag(value: string, index: number, selected: boolean): HTMLDivElement;
    /**
     * Returns the text that should be displayed in the tag element
     */
    getTagText(value: string): string;
    /**
     * Update a given tag
     */
    updateTag(tag: HTMLDivElement, value: any, index: number, selected: boolean): void;
    model: TagsInputModel;
    static class_map: Dict<string>;
}
export declare class ColorsInputModel extends TagsInputBaseModel {
    defaults(): Backbone.ObjectHash;
}
export declare class ColorsInputView extends TagsInputBaseView {
    /**
     * Create the Color tag
     */
    createTag(value: string, index: number, selected: boolean): HTMLDivElement;
    /**
     * Update a given tag
     */
    updateTag(tag: HTMLDivElement, value: any, index: number, selected: boolean): void;
    /**
     * Validate an input tag typed by the user, returning the correct tag type. This should be overridden in subclasses.
     */
    validateValue(value: string): any;
    model: ColorsInputModel;
}
declare abstract class NumbersInputModel extends TagsInputModel {
    defaults(): Backbone.ObjectHash;
}
declare abstract class NumbersInputView extends TagsInputView {
    render(): void;
    /**
     * Returns the text that should be displayed in the tag element
     */
    getTagText(value: string): string;
    /**
     * Validate an input tag typed by the user, returning the correct tag type. This should be overridden in subclasses.
     */
    validateValue(value: string): any;
    abstract parseNumber(value: string): number;
    formatter: (value: number) => string;
}
export declare class FloatsInputModel extends NumbersInputModel {
    defaults(): Backbone.ObjectHash;
}
export declare class FloatsInputView extends NumbersInputView {
    parseNumber(value: string): number;
    model: FloatsInputModel;
}
export declare class IntsInputModel extends NumbersInputModel {
    defaults(): Backbone.ObjectHash;
}
export declare class IntsInputView extends NumbersInputView {
    parseNumber(value: string): number;
    model: IntsInputModel;
}
export {};
//# sourceMappingURL=widget_tagsinput.d.ts.map