/// <reference types="backbone" />
import { DOMWidgetView } from '@jupyter-widgets/base';
import { CoreDescriptionModel } from './widget_core';
import { DescriptionStyleModel, DescriptionView } from './widget_description';
export declare class CheckboxStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class ToggleButtonStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        font_family: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_style: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_variant: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_weight: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_decoration: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class BoolModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class CheckboxModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class CheckboxView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Overridden from super class
     *
     * Update the description span (rather than the label) since
     * we want the description to the right of the checkbox.
     */
    updateDescription(): void;
    /**
     * Update the visibility of the label in the super class
     * to provide the optional indent.
     */
    updateIndent(): void;
    updateTabindex(): void;
    updateTooltip(): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handles when the checkbox is clicked.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    _handle_click(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed. The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    /**
     * Handle message sent to the front end.
     *
     * Used to focus or blur the widget.
     */
    handle_message(content: any): void;
    checkbox: HTMLInputElement;
    checkboxLabel: HTMLLabelElement;
    descriptionSpan: HTMLSpanElement;
}
export declare class ToggleButtonModel extends BoolModel {
    defaults(): Backbone.ObjectHash;
}
export declare class ToggleButtonView extends DOMWidgetView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    update_button_style(): void;
    set_button_style(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed. The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handles and validates user input.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    _handle_click(event: MouseEvent): void;
    preinitialize(): void;
    el: HTMLButtonElement;
    static class_map: {
        primary: string[];
        success: string[];
        info: string[];
        warning: string[];
        danger: string[];
    };
}
export declare class ValidModel extends BoolModel {
    defaults(): Backbone.ObjectHash;
}
export declare class ValidView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
    readout: HTMLSpanElement;
    icon: HTMLElement;
}
//# sourceMappingURL=widget_bool.d.ts.map