/// <reference types="backbone" />
import { CoreDescriptionModel } from './widget_core';
import { DescriptionStyleModel, DescriptionView } from './widget_description';
declare class StringStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class HTMLStyleModel extends StringStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class HTMLMathStyleModel extends StringStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class LabelStyleModel extends StringStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        font_family: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_style: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_variant: {
            selector: string;
            attribute: string;
            default: string;
        };
        font_weight: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_decoration: {
            selector: string;
            attribute: string;
            default: string;
        };
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class TextStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        background: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_size: {
            selector: string;
            attribute: string;
            default: string;
        };
        text_color: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class StringModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class StringView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    content: HTMLDivElement;
}
export declare class HTMLModel extends StringModel {
    defaults(): Backbone.ObjectHash;
}
export declare class HTMLView extends StringView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    content: HTMLDivElement;
}
export declare class HTMLMathModel extends StringModel {
    defaults(): Backbone.ObjectHash;
}
export declare class HTMLMathView extends StringView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     */
    update(): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    content: HTMLDivElement;
}
export declare class LabelModel extends StringModel {
    defaults(): Backbone.ObjectHash;
}
export declare class LabelView extends StringView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
}
export declare class TextareaModel extends StringModel {
    defaults(): Backbone.ObjectHash;
}
export declare class TextareaView extends StringView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    update_placeholder(value?: string): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    updateTabindex(): void;
    updateTooltip(): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle key down
     *
     * Stop propagation so the event isn't sent to the application.
     */
    handleKeyDown(e: Event): void;
    /**
     * Handles key press
     *
     * Stop propagation so the keypress isn't sent to the application.
     */
    handleKeypress(e: Event): void;
    /**
     * Triggered on input change
     */
    handleChanging(e: Event): void;
    /**
     * Sync the value with the kernel.
     *
     * @param e Event
     */
    handleChanged(e: Event): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    textbox: HTMLTextAreaElement;
}
export declare class TextModel extends StringModel {
    defaults(): Backbone.ObjectHash;
}
export declare class TextView extends StringView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    update_placeholder(value?: string): void;
    updateTabindex(): void;
    updateTooltip(): void;
    update(options?: any): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle key down
     *
     * Stop propagation so the keypress isn't sent to the application.
     */
    handleKeyDown(e: Event): void;
    /**
     * Handles text submission
     */
    handleKeypress(e: KeyboardEvent): void;
    /**
     * Handles user input.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    handleChanging(e: Event): void;
    /**
     * Handles user input.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    handleChanged(e: Event): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    protected readonly inputType: string;
    textbox: HTMLInputElement;
}
export declare class PasswordModel extends TextModel {
    defaults(): Backbone.ObjectHash;
}
export declare class PasswordView extends TextView {
    protected readonly inputType: string;
}
/**
 * Combobox widget model class.
 */
export declare class ComboboxModel extends TextModel {
    defaults(): Backbone.ObjectHash;
}
/**
 * Combobox widget view class.
 */
export declare class ComboboxView extends TextView {
    render(): void;
    update(options?: any): void;
    isValid(value: string): boolean;
    handleChanging(e: KeyboardEvent): void;
    handleChanged(e: KeyboardEvent): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    highlightValidState(valid: boolean): void;
    datalist: HTMLDataListElement | undefined;
    isInitialRender: boolean;
}
export {};
//# sourceMappingURL=widget_string.d.ts.map