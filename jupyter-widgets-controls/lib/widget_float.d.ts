/// <reference types="backbone" />
import { CoreDescriptionModel } from './widget_core';
import { BaseIntSliderView, IntRangeSliderView, IntSliderView, IntTextView } from './widget_int';
export declare class FloatModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class BoundedFloatModel extends FloatModel {
    defaults(): Backbone.ObjectHash;
}
export declare class FloatSliderModel extends BoundedFloatModel {
    defaults(): Backbone.ObjectHash;
    initialize(attributes: any, options: {
        model_id: string;
        comm?: any;
        widget_manager: any;
    }): void;
    update_readout_format(): void;
    readout_formatter: any;
}
export declare class FloatLogSliderModel extends BoundedFloatModel {
    defaults(): Backbone.ObjectHash;
    initialize(attributes: any, options: {
        model_id: string;
        comm: any;
        widget_manager: any;
    }): void;
    update_readout_format(): void;
    readout_formatter: any;
}
export declare class FloatRangeSliderModel extends FloatSliderModel {
}
export declare class FloatSliderView extends IntSliderView {
    /**
     * Validate the value of the slider before sending it to the back-end
     * and applying it to the other views on the page.
     */
    _validate_slide_value(x: any): any;
    _parse_value: typeof parseFloat;
}
export declare class FloatLogSliderView extends BaseIntSliderView {
    update(options?: any): void;
    /**
     * Convert from value to exponent
     *
     * @param value the widget value
     * @returns the log-value between the min/max exponents
     */
    logCalc(value: number): number;
    createSlider(): void;
    /**
     * Write value to a string
     */
    valueToString(value: number): string;
    /**
     * Parse value from a string
     */
    stringToValue(text: string | null): number;
    /**
     * this handles the entry of text into the contentEditable label first, the
     * value is checked if it contains a parseable value then it is clamped
     * within the min-max range of the slider finally, the model is updated if
     * the value is to be changed
     *
     * if any of these conditions are not met, the text is reset
     */
    handleTextChange(): void;
    /**
     * Called whilst the slider is dragged, tapped or moved by the arrow keys.
     */
    handleSliderUpdateEvent(values: number[], handle: number): void;
    /**
     * Called when the slider handle is released after dragging,
     * or by tapping or moving by the arrow keys.
     */
    handleSliderChangeEvent(values: number[], handle: number): void;
    /**
     * Called when the slider value has changed.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    handleSliderChanged(values: number[], handle: number): void;
    updateSliderValue(model: any, value: any, options: any): void;
    updateSliderOptions(e: any): void;
    _validate_slide_value(x: any): any;
    _parse_value: typeof parseFloat;
    private _updating_slider;
}
export declare class FloatRangeSliderView extends IntRangeSliderView {
    /**
     * Validate the value of the slider before sending it to the back-end
     * and applying it to the other views on the page.
     */
    _validate_slide_value(x: any): any;
    _parse_value: typeof parseFloat;
    _range_regex: RegExp;
}
export declare class FloatTextModel extends FloatModel {
    defaults(): Backbone.ObjectHash;
}
export declare class BoundedFloatTextModel extends BoundedFloatModel {
    defaults(): Backbone.ObjectHash;
}
export declare class FloatTextView extends IntTextView {
    _parse_value: typeof parseFloat;
    _default_step: string;
    /**
     * Handle key press
     */
    handleKeypress(e: KeyboardEvent): void;
    /**
     * Handle key up
     */
    handleKeyUp(e: KeyboardEvent): void;
}
export declare class FloatProgressModel extends BoundedFloatModel {
    defaults(): Backbone.ObjectHash;
}
//# sourceMappingURL=widget_float.d.ts.map