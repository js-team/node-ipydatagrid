/// <reference types="backbone" />
import { WidgetView } from '@jupyter-widgets/base';
import { CoreDescriptionModel } from './widget_core';
import { DescriptionStyleModel, DescriptionView } from './widget_description';
export declare class SelectionModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectionView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(): void;
    updateTabindex(): void;
    updateTooltip(): void;
    listbox: HTMLSelectElement;
}
export declare class DropdownModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class DropdownView extends SelectionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     */
    update(options?: {
        updated_view?: DropdownView;
    }): void;
    _updateOptions(): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle when a new value is selected.
     */
    _handle_change(): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
}
export declare class SelectModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectView extends SelectionView {
    /**
     * Public constructor.
     */
    initialize(parameters: WidgetView.IInitializeParameters): void;
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     */
    update(options?: {
        updated_view?: WidgetView;
    }): void;
    updateSelection(index?: null | number): void;
    _updateOptions(): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle when a new value is selected.
     */
    _handle_change(): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
}
export declare class RadioButtonsModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class RadioButtonsView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    /**
     * Adjust Padding to Multiple of Line Height
     *
     * Adjust margins so that the overall height
     * is a multiple of a single line height.
     *
     * This widget needs it because radio options
     * are spaced tighter than individual widgets
     * yet we would like the full widget line up properly
     * when displayed side-by-side with other widgets.
     */
    adjustPadding(e: this): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle when a value is clicked.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    _handle_click(event: Event): void;
    /**
     * Handle message sent to the front end.
     */
    handle_message(content: any): void;
    container: HTMLDivElement;
}
export declare class ToggleButtonsStyleModel extends DescriptionStyleModel {
    defaults(): Backbone.ObjectHash;
    static styleProperties: {
        button_width: {
            selector: string;
            attribute: string;
            default: any;
        };
        font_weight: {
            selector: string;
            attribute: string;
            default: string;
        };
        description_width: {
            selector: string;
            attribute: string;
            default: any;
        };
    };
}
export declare class ToggleButtonsModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class ToggleButtonsView extends DescriptionView {
    initialize(options: WidgetView.IInitializeParameters): void;
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: any): void;
    update_style_traits(button?: HTMLButtonElement): void;
    update_button_style(): void;
    set_button_style(): void;
    events(): {
        [e: string]: string;
    };
    /**
     * Handle when a value is clicked.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    _handle_click(event: Event): void;
    private _css_state;
    buttongroup: HTMLDivElement;
}
export declare namespace ToggleButtonsView {
    const classMap: {
        primary: string[];
        success: string[];
        info: string[];
        warning: string[];
        danger: string[];
    };
}
export declare class SelectionSliderModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectionSliderView extends DescriptionView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed.  The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update(options?: {
        updated_view?: WidgetView;
    }): void;
    regenSlider(e: any): void;
    createSlider(): void;
    events(): {
        [e: string]: string;
    };
    updateSelection(): void;
    updateReadout(index: any): void;
    /**
     * Called whilst the slider is dragged, tapped or moved by the arrow keys.
     */
    handleSliderUpdateEvent(values: number[], handle: number): void;
    /**
     * Called when the slider handle is released after dragging,
     * or by tapping or moving by the arrow keys.
     */
    handleSliderChangeEvent(values: number[], handle: number): void;
    /**
     * Called when the slider value has changed.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    handleSliderChanged(values: number[], handle: number): void;
    updateSliderOptions(e: any): void;
    updateSliderValue(model: any, _: any, options: any): void;
    $slider: any;
    slider_container: HTMLDivElement;
    readout: HTMLDivElement;
}
export declare class MultipleSelectionModel extends SelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectMultipleModel extends MultipleSelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectMultipleView extends SelectView {
    /**
     * Public constructor.
     */
    initialize(parameters: WidgetView.IInitializeParameters): void;
    /**
     * Called when view is rendered.
     */
    render(): void;
    updateSelection(): void;
    /**
     * Handle when a new value is selected.
     */
    _handle_change(): void;
}
export declare class SelectionRangeSliderModel extends MultipleSelectionModel {
    defaults(): Backbone.ObjectHash;
}
export declare class SelectionRangeSliderView extends SelectionSliderView {
    /**
     * Called when view is rendered.
     */
    render(): void;
    updateSelection(index?: number[]): void;
    updateReadout(index: number[]): void;
    /**
     * Called when the slider value is changing.
     */
    handleSliderUpdateEvent(values: number[], handle: any): void;
    /**
     * Called when the slider value has changed.
     *
     * Calling model.set will trigger all of the other views of the
     * model to update.
     */
    handleSliderChanged(values: number[], handle: number): void;
    updateSliderValue(model: any, _: any, options: any): void;
    $slider: any;
    slider_container: HTMLDivElement;
    readout: HTMLDivElement;
}
//# sourceMappingURL=widget_selection.d.ts.map