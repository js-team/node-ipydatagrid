/// <reference types="backbone" />
import { DOMWidgetView } from '@jupyter-widgets/base';
import { CoreDOMWidgetModel } from './widget_core';
export declare class ImageModel extends CoreDOMWidgetModel {
    defaults(): Backbone.ObjectHash;
    static serializers: {
        value: {
            serialize: (value: any) => DataView;
        };
    };
}
export declare class ImageView extends DOMWidgetView {
    render(): void;
    update(): void;
    remove(): void;
    preinitialize(): void;
    el: HTMLImageElement;
}
//# sourceMappingURL=widget_image.d.ts.map