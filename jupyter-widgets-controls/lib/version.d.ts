/**
 * The version of the Jupyter controls widget attribute spec that this package
 * implements.
 */
export declare const JUPYTER_CONTROLS_VERSION = "2.0.0";
//# sourceMappingURL=version.d.ts.map