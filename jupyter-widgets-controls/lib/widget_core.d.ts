/// <reference types="backbone" />
import { DOMWidgetModel, WidgetModel } from '@jupyter-widgets/base';
import { DescriptionModel } from './widget_description';
export declare class CoreWidgetModel extends WidgetModel {
    defaults(): Backbone.ObjectHash;
}
export declare class CoreDOMWidgetModel extends DOMWidgetModel {
    defaults(): Backbone.ObjectHash;
}
export declare class CoreDescriptionModel extends DescriptionModel {
    defaults(): Backbone.ObjectHash;
}
//# sourceMappingURL=widget_core.d.ts.map