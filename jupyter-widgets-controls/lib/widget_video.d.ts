/// <reference types="backbone" />
import { DOMWidgetView } from '@jupyter-widgets/base';
import { CoreDOMWidgetModel } from './widget_core';
export declare class VideoModel extends CoreDOMWidgetModel {
    defaults(): Backbone.ObjectHash;
    static serializers: {
        value: {
            serialize: (value: any) => DataView;
        };
    };
}
export declare class VideoView extends DOMWidgetView {
    render(): void;
    update(): void;
    remove(): void;
    preinitialize(): void;
    el: HTMLVideoElement;
}
//# sourceMappingURL=widget_video.d.ts.map