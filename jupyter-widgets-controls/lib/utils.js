// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.
export { uuid, resolvePromisesDict } from '@jupyter-widgets/base';
/**
 * Apply MathJax rendering to an element, and optionally set its text.
 *
 * If MathJax is not available, make no changes.
 *
 * Parameters
 * ----------
 * element: Node
 * text: optional string
 */
export function typeset(element, text) {
    if (text !== void 0) {
        element.textContent = text;
    }
    if (window.MathJax !== void 0) {
        MathJax.Hub.Queue(['Typeset', MathJax.Hub, element]);
    }
}
/**
 * escape text to HTML
 */
export function escape_html(text) {
    const esc = document.createElement('div');
    esc.textContent = text;
    return esc.innerHTML;
}
/**
 * Creates a wrappable Promise rejection function.
 */
export function reject(message, log) {
    return function promiseRejection(error) {
        if (log) {
            console.error(new Error(message));
        }
        throw error;
    };
}
//# sourceMappingURL=utils.js.map