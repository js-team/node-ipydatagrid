/// <reference types="backbone" />
import { ISerializers } from '@jupyter-widgets/base';
import { DescriptionView } from './widget_description';
import { CoreDescriptionModel } from './widget_core';
export interface ISerializedDatetime {
    /**
     * UTC full year
     */
    year: number;
    /**
     * UTC zero-based month (0 means January, 11 means December)
     */
    month: number;
    /**
     * UTC day of month
     */
    date: number;
    /**
     * UTC hour (24H format)
     */
    hours: number;
    /**
     * UTC minutes
     */
    minutes: number;
    /**
     * UTC seconds
     */
    seconds: number;
    /**
     * UTC millisconds
     */
    milliseconds: number;
}
export declare function serialize_datetime(value: Date): ISerializedDatetime | null;
export declare function deserialize_datetime(value: ISerializedDatetime): Date | null;
export declare const datetime_serializers: {
    serialize: typeof serialize_datetime;
    deserialize: typeof deserialize_datetime;
};
export declare class DatetimeModel extends CoreDescriptionModel {
    defaults(): Backbone.ObjectHash;
    static serializers: ISerializers;
}
export declare class DatetimeView extends DescriptionView {
    render(): void;
    /**
     * Update the contents of this view
     *
     * Called when the model is changed. The model may have been
     * changed by another view or by a state update from the back-end.
     */
    update2(model?: Backbone.Model, options?: any): void;
    events(): {
        [e: string]: string;
    };
    private _update_value;
    private _picker_change;
    private _picker_focusout;
    private _datetimepicker;
    private _timepicker;
    private _datepicker;
}
export interface ISerializedNaiveDatetime {
    /**
     * full year
     */
    year: number;
    /**
     * zero-based month (0 means January, 11 means December)
     */
    month: number;
    /**
     * day of month
     */
    date: number;
    /**
     * hour (24H format)
     */
    hours: number;
    /**
     * minutes
     */
    minutes: number;
    /**
     * seconds
     */
    seconds: number;
    /**
     * millisconds
     */
    milliseconds: number;
}
export declare function serialize_naive(value: Date | null): ISerializedNaiveDatetime | null;
export declare function deserialize_naive(value: ISerializedNaiveDatetime): Date | null;
export declare const naive_serializers: {
    serialize: typeof serialize_naive;
    deserialize: typeof deserialize_naive;
};
export declare class NaiveDatetimeModel extends DatetimeModel {
    defaults(): Backbone.ObjectHash;
    static serializers: ISerializers;
}
//# sourceMappingURL=widget_datetime.d.ts.map