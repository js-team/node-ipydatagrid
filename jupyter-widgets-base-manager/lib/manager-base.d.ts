import * as services from '@jupyterlab/services';
import { JSONObject, PartialJSONObject } from '@lumino/coreutils';
import { DOMWidgetView, WidgetModel, WidgetView, DOMWidgetModel, IClassicComm, ICallbacks, IWidgetManager, IModelOptions, IWidgetOptions } from '@jupyter-widgets/base';
/**
 * The control comm target name.
 */
export declare const CONTROL_COMM_TARGET = "jupyter.widget.control";
/**
 * The supported version for the control comm channel.
 */
export declare const CONTROL_COMM_PROTOCOL_VERSION = "1.0.0";
/**
 * Time (in ms) after which we consider the control comm target not responding.
 */
export declare const CONTROL_COMM_TIMEOUT = 4000;
export interface IState extends PartialJSONObject {
    buffers?: IBase64Buffers[];
    model_name: string;
    model_module: string;
    model_module_version: string;
    state: JSONObject;
}
export interface IManagerStateMap extends PartialJSONObject {
    [key: string]: IState;
}
/**
 * Widget manager state.
 *
 * The JSON schema for this is in @jupyter-widgets/schema/v2/state.schema.json.
 */
export interface IManagerState extends PartialJSONObject {
    version_major: number;
    version_minor: number;
    state: IManagerStateMap;
}
export interface IBase64Buffers extends PartialJSONObject {
    data: string;
    path: (string | number)[];
    encoding: 'base64';
}
/**
 * Make all properties in K (of T) required
 */
export type RequiredSome<T, K extends keyof T> = Omit<T, K> & Required<Pick<T, K>>;
/**
 * Manager abstract base class
 */
export declare abstract class ManagerBase implements IWidgetManager {
    /**
     * Modifies view options. Generally overloaded in custom widget manager
     * implementations.
     */
    setViewOptions(options?: any): any;
    /**
     * Creates a promise for a view of a given model
     *
     * #### Notes
     * The implementation must trigger the Lumino 'after-attach' and 'after-show' events when appropriate, which in turn will trigger the view's 'displayed' events.
     *
     * Make sure the view creation is not out of order with
     * any state updates.
     *
     */
    create_view<VT extends DOMWidgetView = DOMWidgetView>(model: DOMWidgetModel, options?: any): Promise<VT>;
    create_view<VT extends WidgetView = WidgetView>(model: WidgetModel, options?: any): Promise<VT>;
    /**
     * callback handlers specific to a view
     */
    callbacks(view?: WidgetView): ICallbacks;
    /**
     * Get a promise for a model by model id.
     *
     * #### Notes
     * If the model is not found, the returned Promise object is rejected.
     *
     * If you would like to synchronously test if a model exists, use .has_model().
     */
    get_model(model_id: string): Promise<WidgetModel>;
    /**
     * Returns true if the given model is registered, otherwise false.
     *
     * #### Notes
     * This is a synchronous way to check if a model is registered.
     */
    has_model(model_id: string): boolean;
    /**
     * Handle when a comm is opened.
     */
    handle_comm_open(comm: IClassicComm, msg: services.KernelMessage.ICommOpenMsg): Promise<WidgetModel>;
    /**
     * Create a comm and new widget model.
     * @param  options - same options as new_model but comm is not
     *                          required and additional options are available.
     * @param  serialized_state - serialized model attributes.
     */
    new_widget(options: IWidgetOptions, serialized_state?: JSONObject): Promise<WidgetModel>;
    register_model(model_id: string, modelPromise: Promise<WidgetModel>): void;
    /**
     * Create and return a promise for a new widget model
     *
     * @param options - the options for creating the model.
     * @param serialized_state - attribute values for the model.
     *
     * @example
     * widget_manager.new_model({
     *      model_name: 'IntSlider',
     *      model_module: '@jupyter-widgets/controls',
     *      model_module_version: '1.0.0',
     *      model_id: 'u-u-i-d'
     * }).then((model) => { console.log('Create success!', model); },
     *  (err) => {console.error(err)});
     *
     */
    new_model(options: IModelOptions, serialized_state?: any): Promise<WidgetModel>;
    /**
     * Fetch all widgets states from the kernel using the control comm channel
     * If this fails (control comm handler not implemented kernel side),
     * it will fall back to `_loadFromKernelModels`.
     *
     * This is a utility function that can be used in subclasses.
     */
    protected _loadFromKernel(): Promise<void>;
    /**
     * Old implementation of fetching widget models one by one using
     * the request_state message on each comm.
     *
     * This is a utility function that can be used in subclasses.
     */
    protected _loadFromKernelModels(): Promise<void>;
    _make_model(options: RequiredSome<IModelOptions, 'model_id'>, serialized_state?: any): Promise<WidgetModel>;
    /**
     * Close all widgets and empty the widget state.
     * @return Promise that resolves when the widget state is cleared.
     */
    clear_state(): Promise<void>;
    /**
     * Asynchronously get the state of the widget manager.
     *
     * This includes all of the widget models, and follows the format given in
     * the @jupyter-widgets/schema package.
     *
     * @param options - The options for what state to return.
     * @returns Promise for a state dictionary
     */
    get_state(options?: IStateOptions): Promise<IManagerState>;
    /**
     * Set the widget manager state.
     *
     * @param state - a Javascript object conforming to the application/vnd.jupyter.widget-state+json spec.
     *
     * Reconstructs all of the widget models in the state, merges that with the
     * current manager state, and then attempts to redisplay the widgets in the
     * state.
     */
    set_state(state: IManagerState): Promise<WidgetModel[]>;
    /**
     * Disconnect the widget manager from the kernel, setting each model's comm
     * as dead.
     */
    disconnect(): void;
    /**
     * Resolve a URL relative to the current notebook location.
     *
     * The default implementation just returns the original url.
     */
    resolveUrl(url: string): Promise<string>;
    inline_sanitize(source: string): string;
    /**
     * The comm target name to register
     */
    readonly comm_target_name = "jupyter.widget";
    /**
     * Load a class and return a promise to the loaded object.
     */
    protected abstract loadClass(className: string, moduleName: string, moduleVersion: string): Promise<typeof WidgetModel | typeof WidgetView>;
    protected loadModelClass(className: string, moduleName: string, moduleVersion: string): Promise<typeof WidgetModel>;
    protected loadViewClass(className: string, moduleName: string, moduleVersion: string): Promise<typeof WidgetView>;
    /**
     * Create a comm which can be used for communication for a widget.
     *
     * If the data/metadata is passed in, open the comm before returning (i.e.,
     * send the comm_open message). If the data and metadata is undefined, we
     * want to reconstruct a comm that already exists in the kernel, so do not
     * open the comm by sending the comm_open message.
     *
     * @param comm_target_name Comm target name
     * @param model_id The comm id
     * @param data The initial data for the comm
     * @param metadata The metadata in the open message
     */
    protected abstract _create_comm(comm_target_name: string, model_id?: string, data?: JSONObject, metadata?: JSONObject, buffers?: ArrayBuffer[] | ArrayBufferView[]): Promise<IClassicComm>;
    protected abstract _get_comm_info(): Promise<{}>;
    /**
     * Filter serialized widget state to remove any ID's already present in manager.
     *
     * @param {*} state Serialized state to filter
     *
     * @returns {*} A copy of the state, with its 'state' attribute filtered
     */
    protected filterExistingModelState(serialized_state: any): any;
    /**
     * Dictionary of model ids and model instance promises
     */
    private _models;
}
export interface IStateOptions {
    /**
     * Drop model attributes that are equal to their default value.
     *
     * @default false
     */
    drop_defaults?: boolean;
}
/**
 * Serialize an array of widget models
 *
 * #### Notes
 * The return value follows the format given in the
 * @jupyter-widgets/schema package.
 */
export declare function serialize_state(models: WidgetModel[], options?: IStateOptions): IManagerState;
//# sourceMappingURL=manager-base.d.ts.map